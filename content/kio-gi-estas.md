+++
title = "Kio estas La Bona Renkontiĝo?"
+++

<p>
    La Bona Renkontiĝo estas libere elŝutebla podkasto-projekto de <a href="https://esperant.io/">esperant.io</a>, la evento-paĝo kun mapo, kiu montras kiaj Esperanto-renkontiĝoj okazas ĉirkaŭ la mondo. <strong>#LaBoRen</strong> celas instrui kaj konsili pri la tuta organiza proceso de la komenca ideo ĝis la detaloj per aŭskultado de diversaj temoj.
</p>

<p>
 La podkastoj starigas multajn konsiderindajn demandojn. Stela rakontas pri siaj spertoj kaj defioj de organizado ĉiam fokusiĝante sur unu temo. La dosieroj estas elŝuteblaj en altkvalita kaj ankaŭ en simpligita versioj por ebligi la aŭskultadon por ĉiu.
</p>