+++
title = "Subtenu LaBoRen!"
+++

<div class="columns">
    <div class="column is-half">
        <div class="card">
            <div class="card-content">
                <p class="title">
                “Invitu la soifan organizanton al la trinkejo por malvarma biero!”
                </p>
                <p class="subtitle">
                ordonis la Moŝta Trinkejestro  </p>
            </div>
            <footer class="card-footer">
                <p class="card-footer-item">
                <span>
                    Subtenu ĉe <a href="https://www.paypal.me/laboren/10">Paypal</a>
                </span>
                </p>
            </footer>
        </div>
    </div>
</div>