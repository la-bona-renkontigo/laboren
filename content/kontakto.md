+++
title = "Kontaktu nin!"
+++
<p>
    Se vi havas demandon rilate al organizado, aŭ vi deziras, ke ni faru podkaston pri ajna teamo rilate al organizado, simple demandu! Ni volonte helpas al vi realigi viajn renkontiĝo-revojn.
</p>

<p>
   <i class="fab fa-twitter-square" style="font-size: 2em; color:  #0084b4"></i>
 <a href="https://twitter.com/lbr_eo/" target="_blank"> #LaBoRen en twitter</a>
</p>
<p>
      <i class="fab fa-facebook" style="font-size: 2em; color:  #3B5998"></i> <a href="https://www.facebook.com/laborenpodkasto" target="_blank">#LaBoRen en facebook</a>
</p>

<p>
    <i class="fab fa-telegram" style="font-size: 2em; color: CornflowerBlue"></i> <a href="https://t.me/LaBonaRen" target="_blank">#LaBoRen informkanalo </a>- tuja mesaĝo pri la plej nova epizodo en telegramo
</p>
<p>
    <i class="fab fa-telegram" style="font-size: 2em; color: CornflowerBlue"></i> <a href="https://t.me/joinchat/EbOV0RI2aECQRfxcaOokmA" target="_blank">#LaBoRen-babilejo en telegramo </a>- demandu, babiladu, prisdiskutu renkontiĝojn kun aliaj organizantoj!

</p>
<p>
Se vi prefere skribus al ni retleteron: <strong>info[@]laboren.org</strong>
</p>
<p>
    Se vi deziras <strong>personan konsilon</strong> rilate al via renkontiĝo, skribu al ni! Ni vidu kiel ni provus solvi vian defion rilate al organizado. 
</p>