+++
title = "Kiel elekti ejon?"
path = "3-kiel-elekti-ejon"
aliases = ["3"]
weight = 3
date = 2017-08-11T15:07:04Z

[extra]
duration = "25:12"
mp3 = { filename = "episodes/LaBoRen-P3-kiel-elekti-ejon-20170811-150227.mp3", size = 25281855 }
mp3_lofi = { filename = "episodes/LaBoRen-P3-kiel-elekti-ejon-malpeza-20170811-150403.mp3", size = 6047451 }
ogg = { filename = "", size = 0 }

[[extra.comments]]
pk = 2
name = "Stela Besenyei-Merger"
color = "hsl(353, 100%, 41%)"
submit_date = "2017-08-22T09:45:22.863861"
comment = """Kiajn spertojn vi havas rilate al "strangaj" manĝaĵoj de via lando, kiu timigis aŭ surprizis viajn partoprenantojn?"""
+++

Kie okazas via renkontiĝo estas unu el la kernaj demandoj de organizado. Depende de tio kian renkontiĝon vi volas fari, vi elektas la ejon. Tio estas la ora regulo: vi devas bone pripensi kion vi ne volos negoci rilate al viaj bezonoj por realigi la renkontiĝon, kiun vi volas.

Junularaj renkontiĝoj kutime havas programon, manĝadon kaj dormadon samloke. La organizantoj zorgas pri ĉiu tri parto de la evento, tio estas baza afero kutime. Se vi ne faras tion, vi bezonas informi viajn partoprenontojn pri tio. Mi donas konsilojn pri kio atenti, kiel pripensi kiel elekti lokon.

