+++
title = "Kial vi organizas?"
path = "1-kial-vi-organizas"
aliases = ["1"]
weight = 1
date = 2017-07-26T12:07:14Z

[extra]
duration = "19:19"
mp3 = { filename = "episodes/1-kial-vi-organizas.mp3", size = 18523532 }
mp3_lofi = { filename = "episodes/LaBoRen-P1-kial-vi-organizas-malpeza-20170726-182929.mp3", size = 4630779 }
ogg = { filename = "episodes/LaBoRen-P1-kial-vi-organizas-20171004-183611.mp3", size = 18523532 }

[[extra.comments]]
pk = 4
name = "Stela Besenyei-Merger"
color = "hsl(353, 100%, 41%)"
submit_date = "2017-08-22T09:49:30.141512"
comment = """Pri kiuj aranĝoj vi laboras? Kion vi organizas momente?"""

[[extra.comments]]
pk = 7
name = "Marko"
color = "hsl(164, 100%, 41%)"
submit_date = "2017-09-29T13:01:22.481717"
comment = """Mi tre aprezas ke vi inkluzivigas ankaŭ novulojn. Denove tre bona podkasto!"""
+++

Mi traktas la plej gravan demandon rilate al la organizado. Kial vi volas organizi? Kiuj estas viaj motivoj? 
Kio certigos, ke vi faros la laboron de la organizado? Antaŭ vi komencas ion ajn fari: pripensu kio estas la celo de via renkontiĝo, ĉar tio difinos viajn taskojn. Kian ejon serĉi, kie organizi, kiom da homoj inviti por kiom da tempo ktp.

Mi komencas la podkaston per komparado de plenkreskaj kaj junularaj renkontiĝoj, ke laŭ mia sperto kiel ili malsimilas. Se vi ankoraŭ ne partoprenis junularan Esperanto-renkontiĝon kiel ili aspektas nuntempe. Fine, ĉu vi emas organizi, sed ne scias kion kaj kiom longe?
Mi donas al vi taskon je la fino de la podkasto, kaj vi havos bonan ekirpunkton por la pensado.

