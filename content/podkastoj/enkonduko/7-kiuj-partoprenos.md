+++
title = "Kiuj partoprenos vian renkontiĝon?"
path = "7-kiuj-partoprenos"
aliases = ["7"]
weight = 7
date = 2017-09-29T10:13:27Z

[extra]
duration = "23:08"
mp3 = { filename = "episodes/LaBoRen-P7-kiuj-partoprenos-20170929-100204.mp3", size = 31104739 }
mp3_lofi = { filename = "episodes/LaBoRen-P7-kiuj-partoprenos-malpeza-20170929-100240.mp3", size = 5551020 }
ogg = { filename = "episodes/LaBoRen-P7-kiuj-partoprenos-20170929-100515.ogg", size = 21418525 }
+++

Kiel organizantoj kiel ni povas pensi pri niaj partoprenantoj? Kiaj kategorioj estas? Ĉu partoprenanto ne estas "simple" partoprenanto?

En tiu ĉi epizodo mi rakontos al vi kiel mi pensas pri la partoprenantoj, kiel tio helpas min pripensi la programerojn, kiel influas tio la organizadon. Havado de klara bildo pri kiujn vi volas inviti helpos vin decidi kion skribi sur vian retpaĝon, kiel inviti kaj kian etoson celi.

Do fakte,  kiuj partoprenos vian renkontiĝon?

