+++
title = "Bazaj demandoj por plani renkontiĝon - resumo"
path = "9-bazaj-demandoj-resumo"
aliases = ["9"]
weight = 9
date = 2017-10-11T10:18:40Z

[extra]
duration = "28:29"
mp3 = { filename = "episodes/LaBoRen-P9-bazaj-demandoj-resumo-20171011-100743.mp3", size = 30323113 }
mp3_lofi = { filename = "episodes/LaBoRen-P9-bazaj-demandoj-resumo-malpeza-20171011-101126.mp3", size = 6836558 }
ogg = { filename = "episodes/LaBoRen-P9-bazaj-demandoj-resumo-20171011-101407.ogg", size = 21685671 }
+++

Ni alvenis al la lasta epizodo de la unua serio de #LaBoRen! En tiu ĉi duonhoro mi uzos la tempon por kunigi la sciindaĵojn de la naŭ epizodoj en la formo de demandoj. Mi resumos tre rapide pri kio mi parolis ĝin nun, kiajn konsiderindaĵojn vi havu antaŭ komenci organizi por doni plenan bildon pri planado de ajna renkontiĝo.

Mi listos al vi la demandojn kiujn vi pripensu paŝo post paŝo por la konstruado de via renkontiĝo. Nun vi jam ekhavas bonan bazon por komenci la laboron. Mi esperas, ke je la fino de tiu ĉi epizodo vi havos klaran ideon kial vi organizas, kaj kion vi volas fari. Ek al la laboro!

