+++
title = "Bonvenon al LaBoRen!"
path = "0-bonvenon"
aliases = ["0"]
weight = 0
date = 2017-07-22T10:36:36Z

[extra]
duration = "12:03"
mp3 = { filename = "episodes/0-bonvenon.mp3", size = 11551137 }
mp3_lofi = { filename = "episodes/LaBoRen-P0-bonvenon-malpeza-20170726-182915.mp3", size = 2887680 }
ogg = { filename = "episodes/LaBoRen-P0-bonvenon-20171004-183648.mp3", size = 11551137 }
[[extra.comments]]
pk = 1
name = "Batisteo"
color = "hsl(7, 100%, 41%)"
submit_date = "2017-08-16T07:30:12.377000"
comment = """Tre bona projekto!<br>
Igas eĉ min voli organizi!<br>
<3"""

[[extra.comments]]
pk = 6
name = "Marko"
color = "hsl(164, 100%, 41%)"
submit_date = "2017-09-25T19:25:28.309708"
comment = """La unua podkasto emigas min aŭskulti la sekvajn!"""

[[extra.comments]]
pk = 9
name = "Jozefo"
color = "hsl(46, 100%, 41%)"
submit_date = "2017-10-01T20:40:18.397038"
comment = """La projekto tre plaĉas al mi! <br>
Ĉu mi rajtas reklami ĝin? :-)"""

[[extra.comments]]
pk = 89
name = "Hasano"
color = "hsl(160, 100%, 41%)"
submit_date = "2018-01-28T06:30:03.170442"
comment = """Saluton! <br>
<br>
Dankon pro la projekto. <br>
Ĝi tre plaĉas al mi kaj pretas kunlabori kun vi por scii multon pri tio. <br>
Mi estas esperantisto de Burundo en Afriko"""

[[extra.comments]]
pk = 91
name = "MIRINDA"
color = "hsl(301, 100%, 41%)"
submit_date = "2018-02-08T14:44:08.349665"
comment = """Pro tio ke mi admiras vin, mi nomumas vin MIRINDA!  Stela Mirindajho el Hungario kiu ĉiam brilas en la esperanta ĉielo, precipe dum Kristnasko kaj nun en la nova ja 2018. Saluton kaj bonvenon al ĉi tiu nova aventuro!"""
+++

Kio estas La Bona Renkontiĝo projekto? Kaj kiu estas mi, Stela? 

En la enkonduka epizodo de la serio mi prezentas min, iomete rakontas pri miaj spertoj en la movado kaj pri la celoj de la projekto.
Kio ĝi estas, kion mi esperas transdoni en la podkastoj. Mi klarigas ke laŭ mi kiaj aranĝoj ekzistas nuntempe por junuloj en la movado, kaj tuj sukcesas eĉ enigi kritikon pri la nuntempaj aktivulaj renkontiĝoj. 

Fine mi instigas vin pripensi kiajn spertojn vi havas kaj espere inspiri vin transdoni ilin al la sekva generacio.

