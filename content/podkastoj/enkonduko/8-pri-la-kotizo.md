+++
title = "Kion enhavas la kotizo?"
path = "8-pri-la-kotizo"
aliases = ["8"]
weight = 8
date = 2017-10-04T12:45:02Z

[extra]
duration = "33:36"
mp3 = { filename = "episodes/LaBoRen-P8-pri-la-kotizo-20171004-124244.mp3", size = 35497202 }
mp3_lofi = { filename = "episodes/LaBoRen-P8-pri-la-kotizo-malpeza-20171004-124418.mp3", size = 8062328 }
ogg = { filename = "episodes/LaBoRen-P8-pri-la-kotizo-20171004-124452.ogg", size = 25691626 }
+++

Kiam vi organizas renkontiĝon vi kutime kalkulas kotizon por pagigi per viaj partoprenantoj.
Kion ĝi enhavas? Kio ĝi estas, kion ĝi ne enhavas?

Tiun komplikan demandon kun siaj kelkfoje malfacilaj reguloj provas mi iom pli simpligi por vi. Kiel ĉiam, gravas kion vi volas organizi kiajn programerojn vi elektas, kaj efektive kiom ili kostas. Ĉio estas demando de planado.

