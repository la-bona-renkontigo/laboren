+++
title = "Kion vi demandu en la aliĝilo?"
path = "6-aligxilo"
aliases = ["6"]
weight = 6
date = 2017-09-13T21:09:23Z

[extra]
duration = "23:42"
mp3 = { filename = "episodes/LaBoRen-P6-aligxilo-20171004-183104.mp3", size = 25844934 }
mp3_lofi = { filename = "episodes/LaBoRen-P6-aligxilo-malpeza-20171004-183131.mp3", size = 5687798 }
ogg = { filename = "episodes/LaBoRen-P6-aligxilo-20171004-183205.ogg", size = 18615382 }


[[extra.comments]]
pk = 67
name = "Ana"
color = "hsl(227, 100%, 41%)"
submit_date = "2017-12-26T16:11:25.452854"
comment = """Via vidpunkto estas pli klara nun! Mi pensis ke vi entute malkonsetus kun tio... Nu mi komprenas ke estas pli facila por la organizantoj.<br>
<br>
Pri homoj kiuj ne aliĝas sed venas, mia sugesto estis simple mencii tion ĉar nu oni devas ankaŭ pensi pri tio dum organizado. Eble je la fino de la podkasto vi povas fari iun resumon parolante ankaŭ pri detaloj kiujn vi eble forgesis."""

[[extra.comments]]
pk = 46
name = "Ana"
color = "hsl(227, 100%, 41%)"
submit_date = "2017-12-23T19:13:22"
comment = """Saluton Stela,<br>
<br>
Mi aŭskultis kaj mi malkonsentas kun afero, mi jam aliĝis al renkontiĝoj pagante la minimuman kotizon sed estas ĉar dum tiu momento mi ne havis la tutan monsumon por pagi la tutan renkontiĝon, sed tamen mi certis ke mi partoprenus... Do mi indikis tion al la organizantoj kaj ne lasi por aliĝi je la lasta minuto nur kiam mi havus la tutan monsumon! Tio estis ĉefe kiam mi gajnis studentan salajreton, do kotizo de esperanta renkontiĝo estis eble duono de mia salajro. Mi kredas ke ĝenerale tiu ebleco pagi nur parton por indiki vian partoprenemon estas bona afero! Alia afero kiun vi forgesis diri estas homoj kiuj ne aliĝas sed venas! Tio ankaŭ okazas.<br>
<br>
Brakumon,<br>
Ana"""

[[extra.comments]]
pk = 48
name = "Stela Besenyei-Merger"
color = "hsl(353, 100%, 41%)"
submit_date = "2017-12-24T00:09:25.537143"
comment = """Saluton Ana!

Dankon por via komento. Jes, vi pravas, indas malferme lasi la aferon, ĝi ja helpas. Mi simple mencias, ke multfoje oni ja povus pagi la tuton. Por la organizantoj tio certe estas pli bona solvo por havi malpli da kontanta mono surloke kaj pro la planado ĝenerale. Kaj jes, por multe da homoj povas esti helpo. Estas ankaŭ por mi. Nesurprize.<br>
<br>
Jes, prave, okazas, ke homoj ne aliĝas sed venas. Mi pensas, ke tio ofte estas akceptata. Mi ne vere konas renkontiĝojn, kie oni ne permesis tion. :-)<br>
<br>
Amike,<br>
Stela"""
+++

Vi havas renkontiĝon, vi volas, ke homoj partoprenu. Sed por ke ili povu informi vin, ke ili fakte volas partopreni, ili devos plenigi aliĝilon por vi.

Kio estu en la aliĝilo? Kio gravas? Kial gravas aliĝilo? Kiujn datumojn vi bezonos, kiujn ne? Kion konsideri? Kiel aliĝilo helpas vian organizadon? Vi ricevos respondon al multe da demandoj en tiu epizodo pri aliĝiloj.

