+++
title = "Helpas min la tuta mondo - intervjuo kun Rob Moerbeek"
path = "105-intervjuo-rob-moerbeek"
aliases = ["105"]
weight = 105
date = 2018-05-21T21:13:51Z

[extra]
duration = "39:27"
mp3 = { filename = "episodes/LaBoRen-P105-intervjuo-rob-moerbeek-20180521-202723.mp3", size = 76673881 }
mp3_lofi = { filename = "episodes/LaBoRen-P105-intervjuo-rob-moerbeek-malpeza-20180521-205357.mp3", size = 18935430 }
ogg = { filename = "episodes/LaBoRen-P105-intervjuo-rob-moerbeek-20180521-210938.ogg", size = 34351659 }
+++

Stela intervjuas Rob Moerbeek, kiu estas konata kiel LA provleganto de Esperantujo. Li laboris 32 jarojn, inter 1969 kaj 2001 kiel oficisto en la Centra Oficejo de UEA kaj poste daŭrigis volontule la laboron. Li estas honora membro de UEA, ILEI, ISEA. Por lia laboro li iĝis Kavaliro dum la Malferma Tago de UEA en aprilo 2018.

El tiu ĉi babilado vi ekscios pli pri 
- kion li pensas pri tio, ke li iĝis Kavaliro en la Ordeno de Oranje Nassau,
- kial gravas la blinduloj al li, kaj kiel ni povas helpi,
- kio bezonatas por bona UK, kio mankas de la UK-oj,
- kiel li kaj lia edzino, Nora helpis al komencantoj dum renkontiĝoj,
- kial li daŭre aktivas, eĉ post 6 jardekoj en la movado.

Por detaloj pri la menciitaj organizoj, temoj, homoj, kaj aliaj movadaj rimarkoj vidu la blogon de Stela:
https://stelachiamnurkritikas.wordpress.com/2018/05/22/helpas-min-la-tuta-mondo-intervjuo-kun-rob-moerbeek/

