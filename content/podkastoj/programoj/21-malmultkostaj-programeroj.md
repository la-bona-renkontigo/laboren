+++
title = "Malmultkostaj programeroj! Sed kiel?"
path = "21-malmultkostaj-programeroj"
aliases = ["21"]
weight = 21
date = 2018-07-12T14:27:33Z

[extra]
duration = "27:02"
mp3 = { filename = "episodes/LaBoRen-P21-malmultkostaj-programeroj-20180712-140942.mp3", size = 58306126 }
mp3_lofi = { filename = "episodes/LaBoRen-P21-malmultkostaj-programeroj-malpeza-20180712-141604.mp3", size = 12975750 }
ogg = { filename = "episodes/LaBoRen-P21-malmultkostaj-programeroj-20180712-141727.ogg", size = 20643157 }
+++

Malmultkostaj programeroj! Bone, sed serioze, kiel fari tion? 

Kompreneble ni scias, ke bezonatas atentado pri la budĝeto: oni bezonas trovi neluksan ejon, kaj manĝon, poste nur prepari malmultkostajn programerojn kaj la tuton orkestri. Sonas facile? Certe ne! 

La fakto, ke junularaj renkontiĝoj estas relative malmultkostaj dankeblas al la sindonemaj organizaj teamoj. Ili provas trovi solvojn por malpliigi la kostojn daŭre sen tro da kompromisoj al la tuta programo.

La baza regulo estas: malbona organizado pli kostas. Mi parolas pri tio, kie vi perdas monon, havas neatenditajn kostojn, se vi ne bone organizas.

Mi detaligas:
- kiel malbona organizado altigas la kostojn por via renkontiĝo,
- kiel eviti tiujn problemojn,
- kiuj estas la programeroj kaj organizaj aferoj kiuj fakte kostas la plej multe en la budĝeto,
- kiel provi malpli kostigi ĝenerale multkostajn programerojn,
- konsilas pri kiu tipaj progameroj estas (preskaŭ) senpagaj,
- proponas bonetosigilojn por via evento.

Ek al la defio de malmultkostaj programeroj!

