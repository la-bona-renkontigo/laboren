+++
title = "Ŝildoj, mapoj kaj surloka informado"
path = "17-sxildoj"
aliases = ["17"]
weight = 17
date = 2018-02-15T09:11:18Z

[extra]
duration = "23:06"
mp3 = { filename = "episodes/LaBoRen-P17-sxildoj-20180207-100048.mp3", size = 30003248 }
mp3_lofi = { filename = "episodes/LaBoRen-P17-sxildoj-malpeza-20180207-100048.mp3", size = 11087202 }
ogg = { filename = "episodes/LaBoRen-P17-sxildoj-20180207-100048.ogg", size = 17547020 }

[[extra.comments]]
pk = 107
name = "Andi"
color = "hsl(352, 100%, 41%)"
submit_date = "2018-04-28T09:22:28.606195"
comment = """Se vi, ĉu en mapo aŭ aliloke, aperigas informon pri iu lokaĵo, ne forgesu mencii, krom en Esperanto, _aldone_ ankaŭ ĝian nomon en la nacia lingvo (kaj per loka alfabeto - eĉ se _vi_ ne povas legi ĝin, helpema lokulo eble pova
s legi _nur_ ĝin!). Tio tre helpas por montri al lokuloj, serĉi en la reto ktp. Mi havas malbonajn memorojn de iuj iel transliterumitaj loknomoj en cirila alfabeto... Poste, kiam mi finfine ekhavis la loknomon oficial-cirillitere, mi apen
aŭ povis legi ĝin, sed finfine trovi la urbon en serĉmaŝino."""
+++

Viaj partoprenantoj jam alvenis kaj akceptiĝis, sed nun bezonas informojn por komenci ekĝui la renkontiĝon. Kio, kie estas? Kie trovi la etoson?

Krom la tre praktikaj konsiloj mi komencos la epizodon per la klarigado kiom da energio vi povas ŝpari por viaj organizantoj per respondado de demandoj antaŭ ili aperas! 

Speciala atento estos en la epizodo pri tio, kial tiu tasko rilate al ŝildoj kaj informado estas unu el la plej bonaj metodoj por envolvi la novulojn de via organiza teamo!

