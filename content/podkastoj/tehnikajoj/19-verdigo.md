+++
title = "Kiel verdigi vian renkontiĝon?"
path = "19-verdigo"
aliases = ["19"]
weight = 19
date = 2018-03-01T08:15:02Z

[extra]
duration = "26:47"
mp3 = { filename = "episodes/LaBoRen-P19-kiel-verdigi-vian-renkonti-20180226-101419.mp3", size = 36993686 }
mp3_lofi = { filename = "episodes/LaBoRen-P19-kiel-verdigi-vian-renkonti-malpeza-20180226-101634.mp3", size = 12856005 }
ogg = { filename = "episodes/LaBoRen-P19-kiel-verdigi-vian-renkonti-20180226-101704.ogg", size = 20990727 }
+++

En tiu fina epizodo de la dua serio vi povos aŭdi ideojn pri kiel pensi pri la naturo, kaj protektado de la medio dum via renkontiĝo.

Kiel atenti pri tio, ke via renkontiĝo kiel eble plej malmulte da rubaĵo produktu? Oni povas fari (pli) ekologian trinkejon kaj gufujon! Kaj kion vi povas fari rilate al viaj programeroj?

Kiel vi povas inspiri viajn partoprenantojn por helpi malpliigi la rubaĵon aŭ almenaŭ iom pli ekologie vojaĝi?
Ankaŭ en tiu epizodo venas multe da konsiloj, pripensidaĵoj kaj demandoj por rekonsideri vian organizstilon.

