+++
title = "Kiel GLAT-igi, alirebligi vian renkontiĝon?"
path = "12-glat-alirebligo"
aliases = ["12"]
weight = 12
date = 2017-12-06T08:56:38Z

[extra]
duration = "33:06"
mp3 = { filename = "episodes/LaBoRen-P12-glat-alirebligo-20171205-164217.mp3", size = 33951103 }
mp3_lofi = { filename = "episodes/LaBoRen-P12-glat-alirebligo-malpeza-20171205-164217.mp3", size = 15887463 }
ogg = { filename = "episodes/LaBoRen-P12-glat-alirebligo-20171205-164217.ogg", size = 24938977 }
+++

Kiel certiĝi, ke viaj partoprenantoj bone fartos dum via renkontiĝo? Venu epizodo numero 2 pri viaj partoprenantoj!

La graveco de bona informado, konduto-reguloj, kaj multe pli... Komprenu, ke vi devas atenti pri tio kio igas viajn partoprenantojn farti (mal)bone dum via renkontiĝo. Multe da malagrablajn okazaĵojn vi povas eviti, se vi bone informas la homojn. Temu pri kostoj, ejo, manĝaĵoj, necesejoj aŭ programeroj.

Legu la ideojn de Egalecen pri tiu ĉi temo: https://egalecen.org/2016/11/26/kvin-metodoj-por-certigi-ke-via-renkontigo-estu-alirebla-kaj-mojosa/
Rimedoj de Egalecen: https://egalecen.org/rimedoj/
Podkasto-epizodo pri alirebleco de Egalecen: https://egalecen.org/2017/10/16/egalecen-podkasto-02-alirebleco/

