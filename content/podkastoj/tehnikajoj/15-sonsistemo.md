+++
title = "Parolu pli laŭte! Sonsistemoj kaj koncertoj"
path = "15-sonsistemo"
aliases = ["15"]
weight = 15
date = 2018-01-24T15:37:39Z

[extra]
duration = "34:59"
mp3 = { filename = "episodes/LaBoRen-P15-sonsistemo-20180124-142148.mp3", size = 43057696 }
mp3_lofi = { filename = "episodes/LaBoRen-P15-sonsistemo-malpeza-20180124-142148.mp3", size = 16793599 }
ogg = { filename = "episodes/LaBoRen-P15-sonsistemo-20180124-142148.ogg", size = 25640743 }
+++

Kiel organizi koncertojn por via renkontiĝo? Kiom da homoj povas maksimume ĝui akustikan koncerton? Kio estas fakte akustika koncerto? Kiujn erarojn ne faru dum la antaŭpreparado de la vespera programo?

Kiel ne kriu al viaj partoprenantoj? Bone, tiu ĉi demando ja estas facile. Neniu ŝatas se oni krias al ili. Sed ĉu vi fakte scias kion enhavas baza sonsistemo kaj kiel funkciigi ĝin por sukcesi eviti la kriadon?

En tiu ĉi longeta epizodo mi petis la helpon de Melono, kiu estas tre sperta sonteknikisto de pluraj junularaj renkontiĝoj, ke li konsilu pri la sonorigado por helpi amatorajn kaj naivajn organizantojn kiel mi. 

Post tiu ĉi epizodo vi havos tre bonan ideon kiel certiĝi, ke via(j) koncerto(j) bonorde okazu. Mi detale pridiskutas pri kio atenti kaj kiel laŭeble plej bone eviti erarojn de tiu parto de la programo. Ĝi ja garantiite estos unu el la plej multkostaj partoj de via budĝeto. Kaj ni ne forgesu pri la nokto? Ĉu vi volas karaokeon aŭ diskejon ankaŭ? Do, aŭskultu tuj!

Ek al la dancado!

