+++
title = "Kiel vi ĝuos vian propran renkontiĝon?"
path = "18-ripozado-kunfesto"
aliases = ["18"]
weight = 18
date = 2018-02-22T13:52:56Z

[extra]
duration = "27:23"
mp3 = { filename = "episodes/LaBoRen-P18-ripozado-gxuado-dum-evento-20180221-140022.mp3", size = 37173457 }
mp3_lofi = { filename = "episodes/LaBoRen-P18-ripozado-gxuado-dum-evento-malpeza-20180221-140048.mp3", size = 13146905 }
ogg = { filename = "episodes/LaBoRen-P18-ripozado-gxuado-dum-evento-20180221-140415.ogg", size = 21287795 }
+++

Ripozado dum via renkontiĝo fakte signifas bonan planadon de la laboro antaŭ via evento komenciĝas.
Kiam vi planas vian laboron, ankaŭ tion planu kiam vi ripozos. Kiel vi dividos la taskojn, ke la organizantoj povu ĝui la renkontiĝon ankaŭ.

Tiun taskon vi povos bone fari, se vi jam estas iom pli sperta organizanto, tamen mi konsilos pri kiel pensi pri tiu ĉi defio. Kiel provi certiĝi pri tio, ke ankaŭ viaj organizantoj povas partopreni, kunfesti kaj ripozi dum la renkontiĝo - ne nur labori, problemsolvi kaj kuri de unu loko al la alia?

Konsiloj de germanaj organizantoj kaj hungaraj spertoj montros al vi diversajn eblecojn por via teamo. Kial ne provi almenaŭ unu-du el ili?

