+++
title = "Kiuj estas en la teamo de LaBoRen?"
+++

<div class="columns">
  <div class="column is-one-third">
    <img src="/img/Stela_Besenyei_Merger.jpg">
  </div>
  <div class="column">
    <p>
      Saluton! Mi estas <a href="https://twitter.com/Stela_Bee">Stela</a>
<i class="fa fa-smile-o" style="vertical-align: baseline"></i>
      <br>
      Mi kreskis kun Esperanto. Mi partoprenas kaj organizas Esperanto-renkontiĝojn ekde mia juneco. En la podkastoj mi transdonas
      al vi paŝo post paŝo miajn spertojn pri organizado de junularaj renkontiĝoj. Por pli bone ekkoni mian laboron
      kaj kion mi celas per miaj #LaBoRen podkastoj aŭskultu la enkondukan epizodon <a href="/0">ĉi-tie</a>.
    </p>
    <p>
      Helpas al mi <a href="https://twitter.com/batisteo">Baptiste</a>, kiu zorgas pri la retejo, kaj Melono, kiu helpas
      per teĥnikaj konsiloj rilate al la surbendado de la epizodoj.
    </p>
  </div>
</div>