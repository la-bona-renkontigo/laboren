self.addEventListener('install', e => {
    e.waitUntil(
        caches.open('laboren-pwa-cache').then(cache => {
            return cache.addAll(['/', '/static/js/navbar.js', '/static/js/comments.js', '/static/css/minimal.css', '/kio-gi-estas/', '/kontakto/', '/laudoj/', '/pri-ni/', '/subteno/', '/14-varbado', '/15-sonsistemo', '/102-jes2017-la-plendoj', '/101-jes2017-laudoj', '/13-retpagxo', '/12-glat-alirebligo', '/11-kiajn-dokumentojn-vi-bezonas', '/10-kiel-bone-kunsidi', '/9-bazaj-demandoj-resumo', '/8-pri-la-kotizo', '/7-kiuj-partoprenos', '/6-aligxilo', '/5-kial-gravas-viziti-la-ejon', '/4-pri-la-mangxado', '/3-kiel-elekti-ejon', '/2-kion-faras-ceforganizanto', '/1-kial-vi-organizas', '/0-bonvenon']);
        })
    );
});
