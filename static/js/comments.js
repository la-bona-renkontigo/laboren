axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
axios.defaults.xsrfCookieName = "csrftoken";

Vue.component('bulma-comment', {
    props: ['comment'],
    template: `
        <div class="media">
        <figure class="media-left">
            <p class="image is-64x64">{{comment.name[0].toUpperCase()}}</p>
        </figure>
        <div class="media-content">
            <div class="content">
                <p><strong>{{comment.name}}</strong> <small>nun</small> <br>
                    {{comment.comment}}
                </p>
            </div>
        </div>
        </div>
    `
})

new Vue({
    el: "#komentoj",

    data: {
      fields: {
        name: '',
        email: '',
        comment: '',

        timestamp: '',
        content_type: '',
        object_pk: '',
        security_hash: '',
      },
      comments: [],
      error400: false,
      error500: false,
    },

    computed: {
        validName() { return Boolean(this.fields.name) },
        validEmail() { return Boolean(this.fields.email) && this.fields.email.includes('@') },
        validComment() { return Boolean(this.fields.comment) },
        disabled() {
            return !(this.validName && this.validEmail && this.validComment)
        }
    },

    methods: {
        submit(e) {
            e.target.classList.add('is-loading');
            const form = this.$refs.form;
            axios.post(form.action, Qs.stringify(this.fields))
                 .then(response => {
                    e.target.classList.remove('is-loading');
                    if (response.data.includes("Dankon por via komento")) {
                        this.comments.push({
                            name: this.fields.name,
                            comment: this.fields.comment,
                        })
                        this.fields.comment = "";
                    }
                    })
                    .catch(error => {
                        this['error' + error.response.status] = true;
                        console.error(`${error.response.statusText} (${error.response.status})`);
                        e.target.classList.remove('is-loading');
                    })
        }
    },

    mounted() {
        if (this.$refs['timestamp']) {
            ['timestamp', 'content_type', 'object_pk', 'security_hash'].map(field => {
                this.fields[field] = this.$refs[field].value
            });
        }
    }

})
